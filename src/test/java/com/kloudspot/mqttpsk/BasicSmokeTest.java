package com.kloudspot.mqttpsk;

import static org.junit.Assert.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Timestamp;
import javax.swing.plaf.SliderUI;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;
import org.junit.BeforeClass;
import org.junit.Test;
import com.kloudspot.mqttpsk.PSKSocketFactory;
import com.kloudspot.mqttpsk.callbacks.TestMqttCallback;

/**
 * Set up mosquitto server and add following to mosquitto.conf: psk_hint mtqq-id psk_file
 * /etc/mosquitto/psk-file
 * 
 * psk-file is: Client_identity:73656372657450534b
 * 
 * @author sjerman
 *
 */
public class BasicSmokeTest implements Serializable {
  private static final String PSK_ID = "Client_identity";
  private static final String PSK_SECRET = "73656372657450534b";
  
  static final String BROKER_URL = "tcp://192.168.0.147:1883";
  static final String TOPIC = "testtopic";
  static final String clientId = "sjerman-pub";

  MqttConnectOptions getConnOpts() {
    HexBinaryAdapter adapter = new HexBinaryAdapter();
    byte[] psk = adapter.unmarshal(PSK_SECRET);

    MqttConnectOptions conOpt = new MqttConnectOptions();
    conOpt.setCleanSession(false);
    conOpt.setSocketFactory(new PSKSocketFactory(PSK_ID.getBytes(), psk));
    return conOpt;
  }

  public void publish(String topicName, int qos, byte[] payload) throws MqttException, InterruptedException {


    MqttClient client = new MqttClient(BROKER_URL, clientId + "p",new MqttDefaultFilePersistence("target/publish"));

    MqttConnectOptions conOpt = getConnOpts();

    // Connect to the MQTT server
    System.out.println("Connecting to " + BROKER_URL + " with client ID " + client.getClientId());
    client.connect(conOpt);
    System.out.println("Connected");

    String time = new Timestamp(System.currentTimeMillis()).toString();
    System.out.println("Publishing at: " + time + " to topic \"" + topicName + "\" qos " + qos);

    // Create and configure a message
    MqttMessage message = new MqttMessage(payload);
    message.setQos(qos);

    // Send the message to the server, control is not returned until
    // it has been delivered to the server meeting the specified
    // quality of service.
    client.publish(topicName, message);

    // Disconnect the client
    client.disconnect();
    System.out.println("Disconnected");
    Thread.sleep(1000);
  }



  @Test
  public void test() throws MqttSecurityException, MqttException, InterruptedException {
    MqttClient client = new MqttClient(BROKER_URL, clientId,new MqttDefaultFilePersistence("target/subscribe"));
    MqttConnectOptions conOpt = getConnOpts();
    client.setCallback(new TestMqttCallback());
    client.connect(conOpt);
    client.subscribe(TOPIC + "/#");

    // send/recieve 5 messages
    int count = 5;
    do {
        publish(TOPIC, 2, "Hello Steve".getBytes());
    } while (count-- > 0);

    try (ByteArrayOutputStream bos = new ByteArrayOutputStream()){
      ObjectOutputStream out = new ObjectOutputStream(bos);   
      out.writeObject(this);
      out.flush();
      byte[] yourBytes = bos.toByteArray();
      
      publish(TOPIC, 2, yourBytes);
      
    } catch (IOException e) {
      e.printStackTrace();
    };
    
    
  }

}
