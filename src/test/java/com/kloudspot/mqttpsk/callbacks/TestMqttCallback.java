package com.kloudspot.mqttpsk.callbacks;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Formatter;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.kloudspot.mqttpsk.TLSPSKClient;


public class TestMqttCallback implements MqttCallback {
  
  private static String cleanTextContent(String text)
  {
      // strips off all non-ASCII characters
      text = text.replaceAll("[^\\x00-\\x7F]", ".");
   
      // erases all the ASCII control characters
      text = text.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", ".");
       
      // removes non-printable characters from Unicode
      text = text.replaceAll("\\p{C}", ".");
   
      return text.trim();
  }

  
  public static String prettyHexView(byte[] ba) {
    
    StringBuilder sb = new StringBuilder();
    Formatter formatter = new Formatter(sb);
    try (ByteArrayInputStream bais = new ByteArrayInputStream(ba) ){
      byte[] b = new byte[64];
      byte[] c;
      int count, n;
      count = 0;
      n = 0;
      StringBuilder sl = new StringBuilder();
      while (bais.read(b,0,1)>0) {
          if (n==0) {
            formatter.format("%08x:",count);
              count +=16;
          }
          n += 1;
          formatter.format(" %02x",b[0]);
          sl.append((char)b[0]);
          if (n==16 || bais.available()==0) {
            String text = cleanTextContent(sl.toString());
            if (text.length()<16) {
             sb.append(String.join("", Collections.nCopies(16-text.length(), "   ")));
            }
            sb.append(" - "+text);
          }
          if (n==16) {
              n=0; 
              sl = new StringBuilder();
              sb.append("\n");
          }          
      }
      bais.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    sb.append("\n");
    return sb.toString();
  }
  
  
  private static Logger LOGGER = LoggerFactory.getLogger(TestMqttCallback.class);
  
  public void deliveryComplete(IMqttDeliveryToken token) {
    LOGGER.info("Delivery complete for: {} ", token.getMessageId());
  }

  public void messageArrived(String topic, MqttMessage message) throws Exception {
    LOGGER.info("Message arrived. Topic: {} Message:\n{} ", topic,prettyHexView(message.getPayload()));
  }

  public void connectionLost(Throwable e) {
    LOGGER.error("Callback error",e);;

  }


}
