package com.kloudspot.mqttpsk;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketAddress;
import java.security.SecureRandom;
import org.bouncycastle.crypto.tls.BasicTlsPSKIdentity;
import org.bouncycastle.crypto.tls.TlsClientProtocol;


public class PskSocket extends Socket {

  private TlsClientProtocol protocol=null;
  private static final SecureRandom secureRandom = new SecureRandom();
  private byte[] pskId;
  private byte[] pskSecret;

  public PskSocket(byte[] pskId, byte[] pskSecret) {
    this.pskId = pskId;
    this.pskSecret = pskSecret; 
  }

  @Override
  public synchronized void close() throws IOException {
    if (protocol != null)
      protocol.close();
  }

  @Override
  public void connect(SocketAddress endpoint) throws IOException {
    Socket s = new Socket();
    s.connect(endpoint);
     _connect(s);
     
  }

  @Override
  public void connect(SocketAddress endpoint, int timeout) throws IOException {
    Socket s = new Socket();
    s.connect(endpoint,timeout);
    
     _connect(s);
  }
  
  private void _connect(Socket s) throws IOException {
    protocol = new TlsClientProtocol(s.getInputStream(), s.getOutputStream(), secureRandom);

     TLSPSKClient client = new TLSPSKClient(pskId,pskSecret);
     protocol.connect(client);
  }

  @Override
  public InputStream getInputStream() throws IOException {
    if (protocol != null) {
      return protocol.getInputStream();
    } else {
      throw new IOException("Socket not open");
    }
  }

  @Override
  public OutputStream getOutputStream() throws IOException {
    if (protocol != null) {
      return protocol.getOutputStream();
    } else {
      throw new IOException("Socket not open");
    }
  }

  @Override
  public void shutdownInput() throws IOException {
    protocol.close();
  }

  @Override
  public void shutdownOutput() throws IOException {
    protocol.close();
  }

}
