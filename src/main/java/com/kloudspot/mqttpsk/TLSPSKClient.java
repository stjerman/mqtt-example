package com.kloudspot.mqttpsk;

import org.bouncycastle.crypto.tls.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Hashtable;

/**
 * https://github.com/rksg/gpb_mqtt_sample_client
 */
public class TLSPSKClient extends PSKTlsClient {
  
    private static Logger LOGGER = LoggerFactory.getLogger(TLSPSKClient.class);

    /**
     * Authentication instance to retrieve server certificate.
     *
     */
    private final TlsAuthentication authentication  = new BCTlsAuthentication();

    /**
     * Hostname to be used in server name indication extension.
     */
    private final String            hostname;

    /**
     * Enabled cipher suites.
     */
    private static final int[]      defaultCS       = new int[] {
            0x008C, 0x008D, 0x00AE, 0x00AF          // TLS_PSK_*
    };

    /*
     * @Override(non-Javadoc)
     *
     * @see org.bouncycastle.crypto.tls.PSKTlsClient#getCipherSuites()
     */
    @Override
    public int[] getCipherSuites() {
        return defaultCS;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.bouncycastle.crypto.tls.AbstractTlsPeer#notifyAlertRaised(short,
     * short, java.lang.String, java.lang.Exception)
     */
    @Override
    public void notifyAlertRaised(short alertLevel, short alertDescription, String message, Throwable cause){
      LOGGER.trace("TLS Alert: Level: {}, description: {}, message: {}",  alertLevel,  alertDescription,  message,  cause);
    }

    @Override
    public void notifySecureRenegotiation(final boolean secureRenegotiation) throws IOException {
      LOGGER.trace("Renegotiated connection: secure: {}",secureRenegotiation);
    }

    /**
     * Create an instance of TLSPSKClient
     *
     * @param hostname
     *            - optional server name for server name indication, SNI will be
     *            omitted if <em>null</em>
     * @param pskId
     *            - pre-shared key username / eID session id
     * @param pskSecret
     *            - corresponding secret for eID session
     */
    public TLSPSKClient(final String hostname, final byte[] pskId, final byte[] pskSecret) {
        super(new TlsPSKIdentity() {

            public void skipIdentityHint() {}

            public void notifyIdentityHint(final byte[] psk_identity_hint) {
              LOGGER.trace("PSK id hint: {}",new String(psk_identity_hint));
            }

            public byte[] getPSKIdentity() {
                return pskId;
            }

            public byte[] getPSK() {
                return pskSecret;
            }
        });
        this.hostname = hostname;
    }

    /**
     * Create an instance of TLSPSKClient with no server name indication.
     *
     * @param pskId
     *            - pre-shared key username / eID session id
     * @param pskSecret
     *            - corresponding secret for eID session
     */
    public TLSPSKClient(final byte[] pskId, final byte[] pskSecret) {
        this(null, pskId, pskSecret);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.bouncycastle.crypto.tls.TlsClient#getAuthentication()
     */
    @Override
    public TlsAuthentication getAuthentication() throws IOException {
        return this.authentication;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.bouncycastle.crypto.tls.AbstractTlsClient#getMinimumVersion()
     */
    @Override
    public ProtocolVersion getMinimumVersion() {
        return ProtocolVersion.TLSv10;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.bouncycastle.crypto.tls.AbstractTlsClient#getClientExtensions()
     */
    @Override
    public Hashtable<Integer, byte[]> getClientExtensions() throws IOException {
        @SuppressWarnings("unchecked")
        Hashtable<Integer, byte[]> clientExtensions = super.getClientExtensions();
        if (clientExtensions == null) {
            clientExtensions = new Hashtable<Integer, byte[]>();
        }

        final ByteArrayOutputStream byteArrOPStream = new ByteArrayOutputStream();
        final DataOutputStream dataOPStream = new DataOutputStream(byteArrOPStream);

        if (this.hostname != null) {
            final byte[] hostnameBytes = this.hostname.getBytes();
            final int snl = hostnameBytes.length;

            // OpenSSL breaks if an extension with length "0" sent, they expect at least
            // an entry with length "0"
            dataOPStream.writeShort(snl == 0 ? 0 : snl + 3); // entry size
            if (snl > 0) {
                dataOPStream.writeByte(0); // name type = hostname
                dataOPStream.writeShort(snl); // name size
                if (snl > 0) {
                    dataOPStream.write(hostnameBytes);
                }
            }

            dataOPStream.close();
            clientExtensions.put(ExtensionType.server_name, byteArrOPStream.toByteArray());
        }

        return clientExtensions;
    }

}

