package com.kloudspot.mqttpsk;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketImpl;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import javax.net.SocketFactory;
import org.bouncycastle.crypto.tls.TlsClientProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PSKSocketFactory extends SocketFactory {
  private static Logger LOGGER = LoggerFactory.getLogger(PSKSocketFactory.class);
  
  private byte[] pskId;
  private byte[] pskSecret;

  public PSKSocketFactory(final byte[] pskId, final byte[] pskSecret) {
    this.pskId = pskId;
    this.pskSecret = pskSecret;
  }

  @Override
  public Socket createSocket() throws IOException {
    return new PskSocket(pskId,pskSecret);
  }

  @Override
  public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
    PskSocket s = new PskSocket(pskId,pskSecret);
    SocketAddress sockaddr = new InetSocketAddress(host, port);    
    s.connect(sockaddr);
    return s;
  }
  
  @Override
  public Socket createSocket(InetAddress host, int port) throws IOException {
    LOGGER.error("createSocket not implemented");
    return null;
  }

  @Override
  public Socket createSocket(String host, int port, InetAddress localHost, int localPort)
      throws IOException, UnknownHostException {
    LOGGER.error("createSocket not implemented");
    return null;
  }

  @Override
  public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort)
      throws IOException {
    LOGGER.error("createSocket not implemented");
    return null;
  }

}
